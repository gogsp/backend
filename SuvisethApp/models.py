from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.conf import settings
from django.db.models.deletion import CASCADE
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.utils.translation import gettext_lazy as _

def upload_to_profile_pics(instance, filename):
    return 'profile_pics/{filename}'.format(filename=filename)

def upload_to_category_pics(instance, filename):
    return 'category_pics/{filename}'.format(filename=filename)

class UserAccountManager(BaseUserManager):
    """Manages for the user profile"""

    def create_user(self, email, username , password=None):
        """Create a new user profile"""
        if not email:
            raise ValueError('User must have an email address')

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            username=username
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, username, password):
        """Create and save a new superuser"""
        user = self.create_user(
            email= self.normalize_email(email),
            username=username,
            password=password
        )
        
        user.is_superuser = True
        user.is_staff = True
        user.is_admin = True
        user.save(using=self._db)

        return user

class UserAccount(AbstractBaseUser, PermissionsMixin):
    """Database model for users in the system"""
    email = models.EmailField(max_length=255,unique=True)
    username = models.CharField(max_length=255,blank=False,null=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_system_admin = models.BooleanField(default=False)
    is_service_provider = models.BooleanField(default=False)
    is_customer = models.BooleanField(default=False)
    

    objects = UserAccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        """ return String representation of the User"""
        return self.email

# for login
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class UserProfile(models.Model):

    #Additional
    DISTRICT = ( 
    ("Ampara", "Ampara"), 
    ("Anuradhapura", "Anuradhapura"),
    ("Badulla","Badulla"),
    ("Baticaloa","Baticaloa"),
    ("Colombo","Colombo"),
    ("Galle","Galle"),
    ("Gampaha","Gampaha"),
    ("Hambantota","Hambantota"),
    ("Jaffna","Jaffna"),
    ("Kalutura","Kalutura"),
    ("Kandy","Kandy"),
    ("Kegalle","Kegalle"),
    ("kilinochchi","kilinochchi"),
    ("Kurunegala","Kurunegala"),
    ("Mannar","Mannar"),
    ("Matale","Matale"),
    ("Matara","Matara"),
    ("Monaragala","Monaragala"),
    ("Mullaitivu","Mullaitivu"),
    ("Nuwara Eliya","Nuwara Eliya"),
    ("Polonnaruwa","Polonnaruwa"),
    ("Puttalam","Puttalam"),
    ("Ratnapura","Ratnapura"),
    ("Trincomalee","Trincomalee"),
    ("Vavuniya","Vavuniya")
    ) 

    GENDER = ( 
    ("Male", "Male"), 
    ("Female", "Female"),
    )

    """ Userprofile model """
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255,blank=True,null=True)
    last_name = models.CharField(max_length=255,blank=True,null=True)
    sur_name = models.CharField(max_length=255,blank=True,null=True)
    image = models.ImageField(
                    _("Image"), upload_to = upload_to_profile_pics, default = 'profile_pics/default.png'
                    )
    gender = models.CharField(
                max_length=10,
                choices=GENDER,
                blank=True, 
                null=True)
    company_name = models.CharField(max_length=150,blank=True,null=True)
    date_of_birth = models.DateField(blank=True,null=True)
    mobile_no = models.CharField(max_length=15,blank=True,null=True)
    address = models.TextField(max_length=250, blank=True,null=True)
    district = models.CharField(
                max_length=150,
                choices=DISTRICT,
                blank=True, 
                null=True)
    
    def __str__(self):
        return self.first_name + ' ' +self.last_name

class Category(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(
                    _("Image"), upload_to = upload_to_category_pics, default = 'category_pics/default.png'
                    )
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


class Package(models.Model):
    categories = models.ManyToManyField(Category)
    users = models.ManyToManyField(UserAccount)
    package_name = models.CharField(max_length=255,blank=False,null=False)
    package_price = models.CharField(max_length=25,blank=False,null=False)
    description = models.CharField(max_length=255,blank=False,null=False)

    def __str__(self):
        return self.package_name


