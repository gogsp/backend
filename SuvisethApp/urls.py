from django.urls import path
from django.urls.conf import include
from .views import registration_view, UserProfileViewSet, UserloginAPIView

from .views import(
    registration_view,
    CategoryList,
    CategoryDetail,
   
)

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

app_name = "SuvisethApp"

router = DefaultRouter()
router.register('user-profile', UserProfileViewSet, basename='user_profile')


urlpatterns = [ 
        path('register',registration_view, name='register'),
        path('login', obtain_auth_token, name='login'),
        path('login2', UserloginAPIView.as_view()),
        path('', include(router.urls)),
        path('categories/', CategoryList.as_view()),
        path('categories/<int:pk>/', CategoryDetail.as_view()),
    
]