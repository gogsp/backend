from django.contrib import admin
from django.db.models import fields
from SuvisethApp import models
    
class UserAccountAdmin(admin.ModelAdmin):
    list_display = ('id','email', 'username', 'is_system_admin', 'is_service_provider', 'is_customer', 'is_admin', 'is_staff')

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name')

admin.site.register(models.UserAccount, UserAccountAdmin)
admin.site.register(models.UserProfile, UserProfileAdmin)
admin.site.register(models.Category)

