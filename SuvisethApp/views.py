from django.http import response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.http import request, response
from rest_framework import status
from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.views import APIView
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework import viewsets
from SuvisethApp.models import UserAccount, UserProfile
from SuvisethApp.permissions import UpdateOwnProfile, UpdateOwnAccount
from .serializers import UserProfileSerializer, UserAccountSerializer
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser
from rest_framework.authentication import TokenAuthentication
from SuvisethApp import models
from SuvisethApp import serializers
from rest_framework.parsers import FormParser, MultiPartParser

from rest_framework.decorators import api_view, permission_classes
from .serializers import CategorySerializer, UserAccountSerializer
from rest_framework.authtoken.models import Token
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView
from rest_framework import permissions
from . import serializers
from . import models
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

@api_view(['POST'])
def registration_view(request):

    if request.method  == 'POST':
        serializer = UserAccountSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            user_account = serializer.save()
            data['response'] = "Successfully registered a new user"
            data['email'] = user_account.email
            data['username'] = user_account.username
            token = Token.objects.get(user=user_account).key
            data['token'] = token
        else:
            data = serializer.errors
        return Response(data)
    
class UserloginAPIView(ObtainAuthToken):
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class UserProfileViewSet(viewsets.ModelViewSet):
    """ Create new customer profile and List all customer profile"""

    authentication_classes = (TokenAuthentication,)
    serializer_class = UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    permission_classes = (
        UpdateOwnProfile,
        IsAuthenticatedOrReadOnly
    )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        

class CategoryList(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (
       IsAdminUser,
    )
    """
    List all categories, or create a new category.
    """
    def get(self, request, format=None):
        categories = models.Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CategoryDetail(APIView):
    """
    Retrieve, update or delete a category instance.
    """
    def get_object(self, pk):
        try:
            return models.Category.objects.get(pk=pk)
        except models.Category.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        category = self.get_object(pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        category = self.get_object(pk)
        serializer = CategorySerializer(category, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["success"] = "update successful"
            return Response(data=data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        category = self.get_object(pk)
        category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
