from django.db.models import fields
from rest_framework import serializers
from rest_framework.serializers import PrimaryKeyRelatedField

from . import models

class UserAccountSerializer(serializers.ModelSerializer):

    password2 = serializers.CharField(style={'input_type':'password'}, write_only=True)

    class Meta:
        model = models.UserAccount
        fields = ['id','email', 'username', 'password', 'password2', 'is_system_admin', 'is_service_provider', 'is_customer', 'is_staff']
        extra_kwargs = {
                'password': {'write_only':True}
        }

    def save(self):
        user_account = models.UserAccount(
                email = self.validated_data['email'],
                username = self.validated_data['username'],
                is_customer = self.validated_data['is_customer'],
                is_system_admin = self.validated_data['is_system_admin'],
                is_service_provider = self.validated_data['is_service_provider'],
                is_staff = self.validated_data['is_staff']
            )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError({'password': 'Password must match'})
        user_account.set_password(password)
        user_account.save()
        return user_account

class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.UserProfile
        fields = ['id', 'first_name', 'last_name', 'sur_name', 'image', 'gender', 'company_name', 'date_of_birth', 'mobile_no', 'address', 'district', 'user']
        extra_kwargs = {
                'user': {'read_only':True}
        } 
   
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model =models.Category
        fields = '__all__'

        def create(self, validated_data):
            return models.Category.objects.create(**validated_data)


